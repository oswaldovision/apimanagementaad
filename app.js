const bodyparser = require('body-parser')
const app = require('express')()
app.use(bodyparser.json())

//routes
const contactRoute = require('./routes/contact')

const port = process.env.PORT || 3000

app.use('/v1/contacts/', contactRoute)

app.listen(port, () => {
  console.log(`Express server listening on port: ${port}`)
})

module.exports = {app}
