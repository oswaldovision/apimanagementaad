const request = require('request')
const secrets = require('./secrets')

const graph = {
  inviteContact: function (tenant, contact) {
    let options = {
      method: 'POST',
      url: 'https://syncaadaval.azurewebsites.net/api/inviteContact?code=mSLwApAW0MRlxvOJx9tg7oJ79fWcVAjP9S34BdALnb8E5k1373Tgrw==',
      headers:
        {
          'Content-Type': 'application/json'
        },
      body:
        {
          'tenant': tenant,
          'contact': contact
        },
      json: true
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  },
  getToken: function (tenantId) {
    let options = {
      method: 'POST',
      url: `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      headers:
        {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      form:
        {
          client_id: secrets.client_id,
          scope: secrets.scope,
          client_secret: secrets.client_secret,
          grant_type: secrets.grant_type
        }
    }

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(JSON.parse(body).access_token)
        }
      })
    })
  }
  ,
  getContacts: function (token) {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/contacts',
      headers:
        {
          'Authorization': token
        }
    }
    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (error) {
          reject(error)
        } else {
          resolve(body)
        }
      })
    })
  }
}

module.exports = graph
