const express = require('express')
const router = express.Router()
const {getToken, getContacts, inviteContact} = require('../graphHelper')
const async = require('asyncawait/async')
const await = require('asyncawait/await')

router.get('/:tenant', function (req, res) {
  let tennantId = req.params.tenant
  getToken(tennantId)
    .then(token => {
      return getContacts(token)
    })
    .then(poolContacts => {
      let contacts = JSON.parse(poolContacts)

      let execInvite = async(function (idtenant, contacts) {
        let setResult = []
        for (let i = 0; i < contacts.length; i++) {
          let user = await(inviteContact(idtenant, contacts[i]))
          setResult.push(user)
        }
        return setResult
      })

      execInvite(tennantId, contacts.value).then(result => {
        res.setHeader('Content-Type', 'application/json')
        res.status(200).send(result)
      })

    })
    .catch(error => {
      res.status(500).send(error)
    })
})

module.exports = router
